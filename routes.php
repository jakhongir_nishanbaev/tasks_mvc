<?php
function call($controller, $action) {
    // require the file that matches the controller name
    require_once('controllers/' . $controller . 'Controller.php');

    // create a new instance of the needed controller
    switch($controller) {
        case 'pages':
            $controller = new PagesController();
            break;
        case 'tasks':
            require_once('models/task.php');
            $controller = new TasksController();
            break;
        case 'auth':
            require_once('models/user.php');
            $controller = new AuthController();
            break;
    }

    // call the action
    $controller->{ $action }();  // ???
}

if (isset($_SESSION['user_admin'])) {
    $tasksActions = array('index', 'show', 'create', 'edit');
}
else {
    $tasksActions = array('index', 'show', 'create');
}

//return var_dump($tasksActions);
// just a list of the controllers we have and their actions
// we consider those "allowed" values
$controllers = array(
    'pages' => ['home', 'error'],
    'tasks' => $tasksActions,
    'auth' => ['login', 'logout'],
);

// check that the requested controller and action are both allowed
// if someone tries to access something else he will be redirected to the error action of the pages controller
if (array_key_exists($controller, $controllers)) {
    if (in_array($action, $controllers[$controller])) {
        call($controller, $action);
    } else {
        call('pages', 'error');
    }
} else {
    call('pages', 'error');
}
?>