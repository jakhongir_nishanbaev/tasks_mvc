<?php
class AuthController {

    public function login() {

        if (isset($_POST['login']) && isset($_POST['password'])) {
            $user = new User();
            $user->login = $_POST['login'];
            $user->password = $_POST['password'];

            if ($user->authenticate()) {
                $_SESSION['user_admin']= $user->login;
                header("Location: http://localhost/tasks_mvc/index.php?controller=tasks&action=index");
            }
        }

        require_once('views/auth/login.php');
    }

    public function logout () {
        $_SESSION['user_admin'] = null;

        header("Location: http://localhost/tasks_mvc/index.php?controller=tasks&action=index");
    }
}
?>