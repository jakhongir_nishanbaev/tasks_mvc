<?php
require 'libs/src/claviska/SimpleImage.php';

class TasksController {
    public function index() {
        $nbOfResults = Task::rowsCount();

        $pagination = [];
        if ($nbOfResults != 0) {
            $entitiesPerPage = 3;

            $nbOfPages = intval($nbOfResults / $entitiesPerPage);

            if( ($entitiesPerPage % $nbOfResults) !== 0 ) {
                $nbOfPages += 1;
            }

            $currentPage = filter_input(INPUT_GET, 'page', FILTER_SANITIZE_NUMBER_INT);

            if (!$currentPage) {
                $currentPage = 1;
            }

            if ($currentPage != 1) {
                $pagination[] = [
                    'page' => 'Назад',
                    'link' => '?controller=tasks&action=index&page=' . ($currentPage - 1),
                    'active' => false,
                ];
            }

            for($i = 1; $i <= $nbOfPages; $i++) {
                $pagination[] = [
                    'page' => $i,
                    'link' => '?controller=tasks&action=index&page=' . $i,
                    'active' => ( $i == $currentPage ),
                ];
            }

            if ($currentPage != $nbOfPages) {
                $pagination[] = [
                    'page' => 'Вперед',
                    'link' => '?controller=tasks&action=index&page=' . ($currentPage + 1),
                    'active' => false,
                ];
            }

        }

        // we store all the tasks in a variable
        $tasks = Task::all();

        require_once('views/tasks/index.php');
    }

    public function show() {
        // we expect a url of form ?controller=posts&action=show&id=x
        // without an id we just redirect to the error page as we need the post id to find it in the database
        if (!isset($_GET['id']))
            return call('pages', 'error');

        // we use the given id to get the right post
        $post = Task::find($_GET['id']);
        require_once('views/tasks/show.php');
    }

    public function create() {
        $linkToImage = null;
        if(isset($_FILES['image']))
        {
            $errors= array();
            $file_name = $_FILES['image']['name'];
            $file_size =$_FILES['image']['size'];
            $file_tmp =$_FILES['image']['tmp_name'];
            $file_type=$_FILES['image']['type'];
            $file_ext=strtolower(end(explode('.',$_FILES['image']['name'])));

            $expensions= array("gif","jpg","png");

            if(in_array($file_ext,$expensions)=== false){
                $errors[]="extension not allowed, please choose a JPEG or PNG file.";
            }

            if($file_size > 2097152) {
                $errors[]='File size must be excately 2 MB';
            }

            if(empty($errors)==true) {
                move_uploaded_file($file_tmp,"storage/images/".$file_name);
                $linkToImage = "storage/images/".$file_name;
                $image = new SimpleImage();
                $image->fromFile($linkToImage);
                $image->resize(320, 240);
                $image->toFile($linkToImage);
//                return var_dump("Success");
            }else{
//                return var_dump($errors);
            }
        }

        if (isset($_POST['user_name']) && isset($_POST['title']) && isset($_POST['email'])) {
            $task = new Task();

            $uploadsDir = 'storage/images';

            if(!is_writable($uploadsDir)){ return var_dump("error in dir") ; }

            $task->title = $_POST['title'];
            $task->user_name = $_POST['user_name'];
            $task->email = $_POST['email'];
            if ($linkToImage != null)
                $task->image = $linkToImage;

            $task->save();

            header("Location: http://localhost/tasks_mvc/index.php?controller=tasks&action=index");
        }

        require_once ('views/tasks/create.php');
    }

    public function edit () {

        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $task = Task::find($id);
        }

        if (isset($_POST['title'])) {
            $id = $_POST['id'];
            $task = Task::find($id);
            $task->title = $_POST['title'];

            if (isset($_POST['status']))
                $task->status = $_POST['status'];
            else
                $task->status = 0;

            $query = $task->update();

            header("Location: http://localhost/tasks_mvc/index.php?controller=tasks&action=index");
        }

        require_once ('views/tasks/edit.php');
    }
}
?>