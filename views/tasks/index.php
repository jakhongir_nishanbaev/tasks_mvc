
<div class="container">
    <div class="row">
        <h3>Список задач</h3>
    </div>
    <div class="row">
    <table class="table table-striped table-bordered" data-toggle="table" data-fixed-columns="false" fixed-table-body="false">
    <thead>
    <tr>
        <th data-field="user_name" data-sortable="true">Имя пользователя</th>
        <th data-field="title" data-sortable="true">Задача</th>
        <th data-field="status" data-sortable="true">Статус</th>
        <th data-field="email" data-sortable="true">E-mail</th>
        <th data-field="image" data-sortable="true">Картинка</th>
        <?php if ($_SESSION['user_admin']) { ?>
            <th>Действия</th>
        <?php } ?>
    </tr>
    </thead>
    <tbody>

    <p>
        <a href="?controller=tasks&action=create" class="btn btn-success">Добавить задачу</a>
    </p>
    <?php foreach($tasks as $task) { ?>
        <tr>
            <p>
            <td><?= $task->user_name; ?> </td>
            <td> <?= $task->title; ?> </td>
            <td>
                <?php if ($task->status)
                echo '<a  href="#"><span class="glyphicon glyphicon-ok-sign" style="color:green"></span></a>';
                else
                    echo '<a  href="#"><span class="glyphicon glyphicon-question-sign" style="color:red; align-self: center"></span></a>';
                ?>
            </td>
            <td> <?= $task->email; ?> </td>
            <td> <img src="<?= $task->image ?>">  </td>

            <?php if ($_SESSION['user_admin']) { ?>
                <td> <a class="btn btn-primary" href="?controller=tasks&action=edit&id=<?= $task->id ?>"> Изменить </a> </td>
            <?php } ?>
            </p>
        </tr>
    <?php } ?>


    </tbody>
</table>


<div class="pagination">
        <?php foreach ($pagination as $p) { ?>
            <li <?php if ($p['active']==true) echo 'class="active"'; ?>> <a href="<?= $p['link'] ?>"> <?= $p['page'] ?> </a></li>
        <?php } ?>
</div>
    </div>
</div>
