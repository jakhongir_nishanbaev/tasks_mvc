<div class="row">
    <div class="col-md-3 col-md-offset-4">
        <form action="?controller=tasks&action=create" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="disabledTextInput">Имя пользователя</label>
                <input id="user_name" name="user_name" type="text" id="disabledTextInput" class="form-control" placeholder="Имя" required>
            </div>

            <div class="form-group">
                <label for="disabledSelect">Задача</label>
                <input id="title" name="title" type="text" id="disabledTextInput" class="form-control" placeholder="Название задачи" required>
            </div>

            <div class="form-group">
                <label for="disabledSelect">Почта</label>
                <input id="email" name="email" type="text" id="disabledTextInput" class="form-control" placeholder="test@test.tur" required>
            </div>

            <div class="form-group">
                <label class="control-label">Выберите файл(JPG/GIF/PNG)</label>
                <input id="image" type="file" name="image" class="file" data-show-preview="false">
            </div>

            <button type="submit" class="btn btn-primary">Добавить</button>
            <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal" onclick="copyInputs();controllImage();">Предпросмотр</button>
        </form>
    </div>
</div>

</div> <!-- /container -->

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Предпросмотр</h4>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-bordered" >
                    <thead>
                    <tr>
                        <th>Имя пользователя</th>
                        <th>Задача</th>
                        <th>Статус</th>
                        <th>E-mail</th>
                        <th>Картинка</th>

                    </tr>
                    </thead>
                    <tbody>

                    <tr>
                        <p>
                        <td id="cell_user_name"> Не заполнено </td>
                        <td id="cell_title"> Не заполнено </td>
                        <td id="cell_status"> <a  href="#"><span class="glyphicon glyphicon-question-sign" style="color:red; align-self: center"></span></a> </td>
                        <td id="cell_email"> Не заполнено </td>
                        <td id="cell_image"> <img width="100" id="image_preview" src="#" alt="your image" />  </td>
                        </p>
                    </tr>

                    </tbody>
                </table>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>

    </div>
</div>

</div>

<script>
    function copyInputs() {
        var userName = document.getElementById("user_name").value;
        var title = document.getElementById("title").value;
        var email = document.getElementById("email").value;
        var image = document.getElementById("image").getAttribute('src');

        document.getElementById("cell_user_name").innerHTML = userName;
        document.getElementById("cell_title").innerHTML = title;
        document.getElementById("cell_email").innerHTML = email;
        document.getElementById("cell_image").setAttribute('src', image);
    }

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#image_preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    function controllImage() {
        if(!$('input[type="file"]').val()) {
            // No file is uploaded, do not submit.

            document.getElementById("image_preview").style.display = "none";
        }
        else {
            document.getElementById("image_preview").style.display = "block";
        }
    }

    $("#image").change(function(){
        readURL(this);
    });
</script>