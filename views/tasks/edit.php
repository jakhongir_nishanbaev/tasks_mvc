<div class="row">
    <div class="col-md-3 col-md-offset-4">
        <form action="?controller=tasks&action=edit" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <input name="id" type="hidden" value="<?= $task->id ?>">
            </div>

            <div class="form-group">
                <label for="disabledSelect">Задача</label>
                <input id="title" name="title" type="text" id="disabledTextInput" class="form-control" placeholder="Название задачи" value="<?= $task->title ?>">
            </div>

            <div class="checkbox">
                <label><input id="checkbox_status" name="status" type="checkbox" <?php if ($task->status==1) echo 'checked'?>>Выполнено</label>
            </div>

            <button type="submit" class="btn btn-primary">Сохранить</button>
        </form>
    </div>
</div>


<script>
    $('#checkbox_status').on('change', function(){
        this.value = this.checked ? 1 : 0;
    }).change();
</script>