<DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet"  href="views/dist/css/bootstrap.min.css" >
<!--        <script src="views/dist/js/bootstrap.min.js"></script>-->
        <script src="views/dist/js/jquery-3.2.1.js"></script>
        <script src="views/dist/js/jquery-3.2.1.min.js"></script>

<!--        <script src="views/dist/js/sortable.js"></script>-->
        <!--        <script src="https://netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css"></script>-->

<!--        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
<!--        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>-->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

        <link href="views/dist/bootstrap-table/1.11.0/bootstrap-table.min.css" rel="stylesheet"/>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.11.0/bootstrap-table.min.js"></script>

    </head>

    <body>

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <ul class="nav navbar-nav">

                <?php if (!isset($_SESSION['user_admin'])) {?>
                    <li class="active"><a href="?controller=auth&action=login">Войти</a></li>
                <?php } else { ?>
                    <li class="active"><a href="?controller=auth&action=logout">Выйти</a></li>
                <?php } ?>

                <li><a href="?controller=tasks&action=index" style="background-color: black; color: white">Задачник</a></li>
            </ul>
        </div>
    </nav>

    <?php require_once('routes.php'); ?>



    <body>
    <html>