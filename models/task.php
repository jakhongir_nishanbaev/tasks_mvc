<?php

class Task {
// we define 3 attributes
// they are public so that we can access them using $task->user_name directly
    public $id;
    public $title;
    public $userName;
    public $status;
    public $email;
    public $image;

    public static $pagination;

    public function __construct($id=null, $title=null, $userName=null, $status=null, $email=null, $image=null) {
        $this->id      = $id;
        $this->title  = $title;
        $this->user_name  = $userName;
        $this->status = $status;
        $this->email = $email;
        $this->image = $image;
    }

    public static function all() {
        $list = [];
        $db = Db::getInstance();
        $req = $db->query('SELECT * FROM tasks')->rowCount();

        $nbOfResults = $db->query('SELECT * FROM tasks')->rowCount();

        if ($nbOfResults != 0) {
            $entitiesPerPage = 3;

            $nbOfPages = intval($nbOfResults / $entitiesPerPage);

            if( ($entitiesPerPage % $nbOfResults) !== 0 ) {
                $nbOfPages += 1;
            }

            $currentPage = filter_input(INPUT_GET, 'page', FILTER_SANITIZE_NUMBER_INT);

            if (!$currentPage) {
                $currentPage = 1;
            }

            $query = "SELECT * FROM tasks ORDER BY id DESC LIMIT " . $entitiesPerPage . " OFFSET " . ( $currentPage - 1 ) * $entitiesPerPage;

            $sth = $db->prepare($query);
            $sth->execute();

            $req = $sth;
        }
        else {
            $query = "SELECT * FROM tasks";

            $sth = $db->prepare($query);
            $sth->execute();

            $req = $sth;
        }

// we create a list of Task objects from the database results
        foreach($req->fetchAll() as $task) {
            $list[] = new Task($task['id'], $task['title'], $task['user_name'], $task['status'], $task['email'], $task['image']);
        }

        return $list;
    }

    public static function find($id) {
        $db = Db::getInstance();
// we make sure $id is an integer
        $id = intval($id);
        $req = $db->prepare('SELECT * FROM tasks WHERE id = :id');
// the query was prepared, now we replace :id with our actual $id value
        $req->execute(array('id' => $id));
        $task = $req->fetch();

        return new Task($task['id'], $task['title'], $task['user_name'], $task['status'], $task['email']);
    }

    public function save () {
        $db = Db::getInstance();

        $query = "INSERT INTO tasks (title, user_name, status, email, image) VALUES ('$this->title', '$this->user_name', '$this->status', '$this->email', '$this->image')";

        $req = $db->prepare($query);
        $req->execute();
    }

    public function update () {
        $db = Db::getInstance();

        $query = "UPDATE tasks SET title='$this->title', status=$this->status WHERE id = '$this->id'";

        $req = $db->prepare($query);
        $req->execute();

        return $query;
    }

    public static function rowsCount() {
        $db = Db::getInstance();
        $count = $db->query('SELECT * FROM tasks')->rowCount();

        return $count;
    }

}
?>